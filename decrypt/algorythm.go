package decrypt

func UnNimbus(s string) string {
	uncryptedString := ""
	for _, ch := range s {
		uncryptedString += string(ch - 1)
	}
	return uncryptedString
}