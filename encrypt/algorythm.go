package encrypt

func Nimbus(s string) string {
	encryptedStr := ""
	for _, c := range s {
		asciiCode := int(c)
		character := string(asciiCode + 1)
		encryptedStr += character
	}
	return encryptedStr
}