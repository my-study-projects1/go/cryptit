package main

import (
	"fmt"
	"gitlab.com/my-study-projects1/go/cryptit/encrypt"
	"gitlab.com/my-study-projects1/go/cryptit/decrypt"
)

func main() {
	s := "asdfgh"

	encStr := encrypt.Nimbus(s)
	decStr := decrypt.UnNimbus(encStr)

	fmt.Println(s, encStr, decStr)

}